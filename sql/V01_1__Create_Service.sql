CREATE TABLE service (
    id int not null auto_increment,
    code varchar(5),
    name varchar(30),
    PRIMARY KEY (id)
);

insert into service(code, name)
values('00001', 'ตรวจสุขภาพประจำปี');

insert into service(code, name)
values('00002', 'จองวัคซีน');
