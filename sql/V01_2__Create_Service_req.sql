CREATE TABLE service_req (
    id int not null auto_increment,
    user_id int not null,
    service_id int not null,
    req_date date,
    req_time varchar(5),
    note varchar(300),
    status varchar(1),
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (service_id) REFERENCES service(id)
);
