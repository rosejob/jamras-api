import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { LoginDTO } from './login.dto';
import { UserDTO } from './user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Post()
  createUser(@Body() body: UserDTO) {
    return this.userService.createUser(body);
  }
  // http://localhost:3000/user/login
  @Post('login')
  login(@Body() body: LoginDTO) {
    return this.userService
      .login(body)
      .then((resp: any) => resp)
      .catch((reason: any) => reason);
  }

  @Get()
  filter(@Query('filter') _filter: string) {
    console.log(_filter);
    const filter 
  }

}
