import { Injectable } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/entities/user.entity';
import { Repository } from 'typeorm';
import { LoginDTO } from './login.dto';
import { UserDTO } from './user.dto';

@Injectable()
export class UserService {
  constructor(@InjectRepository(User) private userRepo: Repository<User>) {}

  createUser(user: UserDTO) {
    const usr = this.userRepo.create(user);
    usr.active = 'Y';
    usr.role = 'A';
    return this.userRepo.save(usr);
  }

  login(userLogin: LoginDTO) {
    const { email, pwd } = userLogin;
    this.userRepo.findOne({ email,pwd, active: 'Y'}).then((user) => {
        if(user) {
          return Promise.resolve({success: true, token: 'MOCK'});
        } else {
          return Promise.reject({
            success: false,
            message: 'User not found',
            msgCode: 'AUTH01',
          });
        }

        

      }
    )
    
    filter(userFilter: UserFilter) {
      const {code, name,email,role,active } = userFilter;
      return this.userRepo
        .createQueryBuilder('u')
        .andWhere('u.code like :code', {code})
        .andWhere('u.code like :code', {code})
        .andWhere('u.code like :code', {code})
        .andWhere('u.code like :code', {code})
        .andWhere('u.code like :code', {code})
        .getMany();
    }

  }

}
